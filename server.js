const app = require('./app');
const socket = require('./socket');
const config = require('./config');

if(config.developerMode){
    var https = require('http').createServer(app);
} else {
    httpsOptions = {
        key: fs.readFileSync('/root/private.key'),
        cert: fs.readFileSync('/root/certificate.cer')
    }
    var https = require('https').createServer(httpsOptions, app);
}

appSocket = new socket(https);

https.listen(config.express.port, config.express.hostname, () => {
    console.log('Server running in port: ', config.express.port);
});

appSocket.listen();
