const db = require('../database/db');
const ajv = require('../libs/ajv');
const throwAjvError = require('../libs/throwAjvError').throwAjvError;

const table="sensor";

const sensors = function(socket){
  
    socket.on('iot/sensors',data=>{
        var schema = {
            "$async": true,
            "additionalProperties": false,
            "type": "object",
            "properties": {
                "sensorId"        : { "type": "string",  "format": "uuid" },
                "sensor" : { "type":"string", },
                "value":{"type":"number"}
            }
        };
        ajv.validate(schema, data).then((valid) => {
            db.sensor.insert(table, data).then((result) => {
                emit(result);
            }, (error) => {
                console.log(error);
            });
        }, (error) => {
            console.log(error);
        });
    });

    function emit(data){
        socket.broadcast.emit("dashboard/sensors",data);
    }
}

module.exports = sensors;