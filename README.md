# Iot API

Descripción de la API
* Usa rethinkdb
* Usa md5 hashing para las contraseñas
* Tiene token basados en autentificación

## Requerimientos

* Instalar y ejecutar rethinkdb.
* Ejecutar el comando  `npm install`.

## Comandos

* Para incializar la base de datos  `npm run db`.
* Para iniciar el servicio `npm start`.

# Estructura del GIT.
Actualmente este repositorio cuenta con la siguiente estructura:

* __db__- Aquí se encuentran archivos con funciones que realizan las querys a la DB.
* __libs__- Aquí se encuentran funciones de apoyo, como las keywords de ajv y una función para parsear errores.
* __routes__- Aquí se encuentran archivos que contienen las rutas de la API.
* __sockets__- Aquí se encuentran archivos con funciones para el uso de socket.io.
* __.gitignore__- Aquí colocamos el listado de carpetas y archivos que no deben estar en el repo.
* __config.js__- Aquí colocamos las variables que se modifican en cada servidor, como la DB y puertos utilizados.
* __createDB.js__- Script que crea la base de datos a utilizar (la borra si ya está creada).
* __package.json__- Metadatos usuales, relevantes para el proyecto.
* __README.md__- Este documento.
* __server.js__- Archivo principal para correr la API.

# URLs importantes
| URL | Descripción |
|-----|-------------|
| https://localhost:3000/iot/api| iot API |
| http://localhost:8080 | Acceso a la Base de Datos |
