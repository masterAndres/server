const express = require('express');
const bodyParser = require('body-parser'); //parser of the body
const db = require('../database/db');
const ajv = require('../libs/ajv');
const throwAjvError = require('../libs/throwAjvError').throwAjvError;

const table="users";

const router = express.Router();
router.use(bodyParser.json({ limit: '15mb' }));
router.use(bodyParser.urlencoded({ limit: '15mb', extended: true }));

router.post('/create', (req, res) => {
    var data = req.body;
    var schema = {
        "$async": true,
        "additionalProperties": false,
        "type": "object",
        "properties": {
            "birthdate": { "type": "string", "format": "date-time"},
            "email"    : { "type": "string", "format": "email"},
            "name"     : { "type": "string"},
            "password" : { "type": "string"},
            "phone"    : { "type": "string", "pattern": "^\\d{10}$"},
            "roleId"   : { "type": "string", "format": "uuid", "validateExistence": { "table":"roles", "property":"id"} },
            "username" : { "type": "string"}
        },
        "required":[ "birthdate", "email", "name", "password", "phone", "roleId", "username" ],
        "validateUniqueness": {"table": table,  "properties":"username"}
    };
    ajv.validate(schema, data).then((valid) => {
        data.active=true;
        db.users.create(table, data).then((result) => {
            res.json({result:result});
        }, (error) => {
            res.status(500).json({error: 'SomethingWrongInServer', params:error});
        });
    }, (error) => {
        console.log(error);
        throwAjvError(error,res);
    });
});

router.post('/get', (req, res) => {
    var data = req.body;
    var schema = {
        "$async": true,
        "additionalProperties": false,
        "type": "object",
        "properties": {
            "id"        : { "type": "string",  "format": "uuid" },
            "pageSize"  : { "type": "integer", "minimum": 1 },
            "pageNumber": { "type": "integer", "minimum": 1 },
            "startDate" : { "type":"string", "format":"date-time"},
            "endDate"   : { "type":"string", "format":"date-time"},
            "active"    : { "type":"boolean"},
            "properties": { "validateProperties": { "table": table }, "type": "array", "items" : { "type":"string" }}
        },
        "dependencies": {
            "pageSize"  : ["pageNumber"],
            "pageNumber": ["pageSize"],
            "endDate"   : ["startDate"]
        }
    };
    ajv.validate(schema, data).then((valid) => {
        db.users.get(table, data).then((result) => {
            res.json({result:result});
        }, (error) => {
            res.status(500).json({error: 'SomethingWrongInServer', params:error});
        });
    }, (error) => {
        throwAjvError(error,res);
    });
});

router.get('/search', (req, res) => {
    var searchItem = req.query.searchItem;
    db.common.search(table, searchItem).then((result) => {
        res.json({result:result});
    }, (error) => {
        res.status(500).json({error: 'SomethingWrongInServer', params:error});
    });  
});

router.post('/edit', (req, res) => {
    var data = req.body;
    var schema = {
        "$async": true,
        "additionalProperties": false,
        "type": "object",
        "properties": {
            "id"       : { "type": "string", "format": "uuid" },
            "birthdate": { "type": "string", "format": "date-time"},
            "email"    : { "type": "string", "format": "email"},
            "name"     : { "type": "string"},
            "password" : { "type": "string"},
            "phone"    : { "type": "string", "pattern": "^\\d{10}$"},
            "roleId"   : { "type": "string", "format": "uuid", "validateExistence": { "table":"roles", "property":"id"} },
            "username" : { "type": "string"}
        },
        "required":["id"],
        "validateUniqueness": {"table": table,  "properties": "username"}
    };
    ajv.validate(schema, data).then((valid) => {
        db.users.update(table, data).then((result) => {
            res.json({result:result});
        }, (error) => {
            res.status(500).json({error: 'SomethingWrongInServer', params:error});
        });
    }, (error) => {
        throwAjvError(error,res);
    });
});

module.exports = router;