const express = require('express');
const bodyParser = require('body-parser'); //parser of the body
const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
const db = require('../database/db');
const config = require('../config');
const Ajv = require('ajv');
const ajv = new Ajv();
const throwAjvError = require('../libs/throwAjvError').throwAjvError;

const publicsRouter = express.Router();
publicsRouter.use(bodyParser.json());

publicsRouter.post('/authenticate', (req, res) => {

    var data = req.body;
    var schema = {
        "$async": true,
        "additionalProperties": false,
        "type": "object",
        "properties": {
            "username": { "type": "string" },
            "password": { "type": "string" }
        },
        "required": ["username", "password"]
    };

    ajv.validate(schema, data).then((valid) => {
        db.users.existUserByUsername(data.username).then((user) => {
            if (user) {
                db.users.validateUser(user.id, data.password, user.active).then((result) => {
                    if (result.password == true) {
                        if (result.active) {
                            var log = {
                                user: user.id,
                                type: "login"
                            };
                            var token = jwt.sign({ id: user.id, name: user.name, permissions: user.permissions }, config.secretKey, {
                                // expiresIn:  31536000 // expires in 1 year.
                                expiresIn: 28800 // expires in 8 hours
                            });
                            res.json({ message: "Token Provided", result: { token: token, permissions: user.permissions } })
                        } else {
                            res.status(401).json({
                                error: 'InvalidCredentials', dataPath: ".status"
                            });
                        }
                    } else {

                        res.status(401).json({ error: 'InvalidCredentials', dataPath: ".password" });
                    }
                }, (err) => {
                    console.log(err);
                    res.status(500).json({ error: "SomethingWrongInServer", params: error });
                });
            } else res.status(404).json({ error: 'notFoundUsername', dataPath: ".username" });
        }, (err) => {
            console.log(err);
            res.status(500).json({ error: "SomethingWrongInServer", params: error });
        });
    }, (error) => {
        if (error instanceof Ajv.ValidationError) {
            throwAjvError(error, res);
        } else {
            console.log(error);
            res.status(500).json({ error: "SomethingWrongInServer", params: error });
        }
    });
});

module.exports = publicsRouter;
