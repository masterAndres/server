const express = require('express');
const bodyParser = require('body-parser'); //parser of the body
const db = require('../database/db');
const ajv = require('../libs/ajv');
const throwAjvError = require('../libs/throwAjvError').throwAjvError;

const table="sensor";

const router = express.Router();
router.use(bodyParser.json({ limit: '15mb' }));
router.use(bodyParser.urlencoded({ limit: '15mb', extended: true }));

router.post('/get', (req, res) => {
    var data = req.body;
    var schema = {
        "$async": true,
        "additionalProperties": false,
        "type": "object",
        "properties": {
            "id"        : { "type": "string",  "format": "uuid" },
            "min" : { "type":"number"}
        },
        "required": ["id", "min"]
    };
    ajv.validate(schema, data).then((valid) => {
        db.sensor.get(table, data).then((result) => {
            res.json({result:result});
        }, (error) => {
            res.status(500).json({error: 'SomethingWrongInServer', params:error});
        });
    }, (error) => {
        throwAjvError(error,res);
    });
});

router.get('/all', (req, res) => {
        db.sensor.all(table).then((result) => {
            res.json({result:result});
        }, (error) => {
            res.status(500).json({error: 'SomethingWrongInServer', params:error});
        });

});


module.exports = router;