module.exports = {
  "express": {
    "hostname": "localhost",
    "port": 3000
  },
  "rethinkdb": {
    "host": "localhost",
    "port": 28015,
    "authKey": "",
    "db": "iotserver"
  },
  "routesPrefix":"/iot/api",
  "developerMode": true,
  "secretKey": "ñla8923e67f"
};