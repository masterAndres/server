const bodyParser = require('body-parser'); //parser of the body
const cors = require('cors');
const middleware = require('./libs/middleware');
const morgan = require('morgan');//logging dev
const express = require('express'); //routing and middleware functions
const config = require('./config');
const app = express();


app.use(cors());
app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

let routesPrefix = '';
if(config.developerMode) {
  routesPrefix = config.routesPrefix; 
} else {
  app.use((req, res, next) => { 
    if(req.headers.host != config.express.hostname)
      res.status(404).json({error: 'requestNotAllowed'});
    else
      next();
  });
}

//#region public routes section
// Rutas públicas, tienen que ir aquí antes de los middleware de decoded y permisos.
app.use(routesPrefix, require('./routes/publics'));
//#endregion

//#region middlewares section
var routes = [];
app.use(middleware.routesValidator(routes));
app.use(middleware.tokenValidator); // token validator
app.use(middleware.permissionsValidator);
//#endregion

//#region private routes section
// Aquí van las rutas...
app.use(routesPrefix + '/users', require('./routes/users'));
app.use(routesPrefix + '/sensor', require('./routes/sensors'));
//#endregion

require('./libs/listRoutes')(app._router.stack).map(i => routes.push(i));

module.exports = app;