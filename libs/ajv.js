var Ajv = require('ajv');
var ajv = new Ajv();
var db = require('../database/db');

ajv.ValidationError = Ajv.ValidationError;

ajv.addKeyword('validateUniqueness', {
    async: true,
    validate: (schema, data, parentSchema, currentPath, parentObject, propertyInParentObject, rootData) => new Promise((resolve, reject) => {
        if(schema.table && schema.properties){
            var id = (data['id'])? data.id : null;
            var aux = Object.assign({}, data);
            //Verificamos que las propiedades que se pasaron en el schema coincidan con las de data.
            for(var key in data){
                var keyExists=false;
                if(Array.isArray(schema.properties)){
                    schema.properties.forEach(element => {
                        if(key===element){keyExists=true;}
                    });}
                else{ 
                    if(key===schema.properties){keyExists=true;}
                }
                if(!keyExists){delete aux[key];}
            }
            //Si alguna propiedad sobrevivió...
            if(Object.keys(aux).length>0){
                //Si se pasó un id entonces se trata de un edit
                if(id){
                    db.common.getById(schema.table, id).then((itemToEdit) => {
                        //Verificamos que el id sea correcto
                        if(itemToEdit){
                            Object.assign(itemToEdit, aux);
                            for(var key in itemToEdit){
                                var keyExists=false;
                                if(Array.isArray(schema.properties)){
                                    schema.properties.forEach(element => {
                                        if(key===element){keyExists=true;}
                                    });}
                                else{ 
                                    if(key===schema.properties){keyExists=true;}
                                }
                                if(!keyExists){delete itemToEdit[key];}
                            }
                            db.common.get(schema.table, itemToEdit).then((matchingItems) => {
                                if(matchingItems.totalFound>0 && (matchingItems.items[0].id !=id)){
                                    reject(new Ajv.ValidationError([{
                                        keyword: "alreadyExists",
                                        dataPath:  "."+schema.properties
                                    }]));
                                    
                                } else {
                                    resolve(true);
                                } 
                            })}
                        else{
                            reject(new Ajv.ValidationError([{
                                keyword: "idDoesNotExist",
                                dataPath: ".id",
                                params:  data.id
                            }]));
                        }
                    })
                }
                else{
                    db.common.get(schema.table, aux).then((matchingItems) => {
                        if(matchingItems.totalFound>0){
                            reject(new Ajv.ValidationError([{
                                keyword: "alreadyExists",
                                dataPath:  "."+schema.properties
                            }]));
                        } else {
                            resolve(true);
                        }   
                    })
                }        
            }
            else{
                console.log("Las propiedades especificadas no fueron provistas.")
                resolve(true);
            }
        }
        else{
            console.log("Schema no provisto de tabla o propiedades.")
            resolve(true);
        }
    })
});

ajv.addKeyword('validateProperties', {
    async: true,
    validate: (schema, data, parentSchema, currentPath, parentObject, propertyInParentObject, rootData) => new Promise((resolve, reject) => {
        if(data.length === 0){
            delete parentObject.properties;
            resolve(true);
        }
        else if(schema.table){
            db.common.getProperties(schema['table']).then((result) => {
            var enumSchema = { 
                "type": "array",    
                "items" : {
                    "type" : "string",
                    "enum" : result
                }
            };
            if(ajv.validate(enumSchema, data)){resolve(true);}
            else{
                reject(new Ajv.ValidationError([{
                    keyword: "invalidProperty",
                    dataPath: currentPath
                }]));
            }
            });
        }
        else{
            resolve(true);
        }
    })
});

ajv.addKeyword('validateExistence', {
    async: true,
    validate: (schema, data, parentSchema, currentPath, parentObject, propertyInParentObject, rootData) => new Promise((resolve, reject) => {
        if(schema.table && schema.property){
            var filter={};
            filter[schema.property]=data;
            db.common.get(schema.table, filter).then((result) => {
                if(result.totalFound>0){
                    resolve(true);
                } else {
                    reject(new Ajv.ValidationError([{
                        keyword: "itemDoesNotExist",
                        params:  schema.properties
                    }]));;
                }
            });
        }
        else{
            console.log("Schema no provisto de tabla o propiedades.")
            resolve(true);
        }
    })
});


module.exports = ajv;