function throwAjvError(error, res) {
    // console.log('error:\n', error);
    // console.log('error.errors[0]:\n', error.errors[0]);
    var err = {
        error: error.errors[0].keyword,
        dataPath: error.errors[0].dataPath,
        params: error.errors[0].params
    }
    console.log(err)
        if (err.error == 'required') {
            err.dataPath = '.' + err.params.missingProperty;
            err.params = undefined;
        } else if (err.error == 'additionalProperties') {
            err.dataPath = '.' + err.params.additionalProperty;
            err.params = undefined;
        } else if (err.error == 'SomethingWrongInServer') {
            err.dataPath = undefined;
        } else if (err.error == 'uniqueItems') {
            err.params = undefined;
        }
        res.status(400).json(err);
    };

module.exports.throwAjvError = throwAjvError;
