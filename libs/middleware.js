const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
const config = require('../config');

// Middleware for route validation
exports.routesValidator = (routes) => {
  return (req, res, next) => {
    // Verificar que la ruta existe
    if(routes.includes(req._parsedUrl.pathname)) {
      next();
    } else {
      res.status(400).json({message: 'route not Found', error: 'incorrectRoute'});
    }
  }
}

// Middleware for token validation
exports.tokenValidator = (req, res, next) => {
  // check header or url parameters or post parameters for token
  if(req.headers && req.headers['refer']) {
    var t = req.headers['refer'].split('?token=%');
    t = t[1];
  }
  var token = req.query.token || req.headers['token'] || t;

  // decode token
  if(token) {
    // verifies secret and checks exp
    jwt.verify(token, config.secretKey, (err, decoded) => {
      if(err) {
        res.status(401).json({message: 'invalid token', error: 'InvalidToken'});
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;
        next();
      }
    });

  } else {
    res.status(400).json({message: 'token not provided', error: 'TokenNotProvided'});
  }
}

// Middleware for check permissions
exports.permissionsValidator = (req, res, next) => {
  let paths = req._parsedUrl.pathname.split('/');
  paths.shift();
  // [template, api, coleccionrutas, laruta]
  // Check decoded token for right permissions.
  let routeGroup = paths[paths.length - 2];
  let currentRoute = routeGroup + '/' + paths[paths.length - 1];
  if(req.decoded.permissions.all || req.decoded.permissions[routeGroup] || req.decoded.permissions[currentRoute]) {
    next();
  }
  else {
    res.status(401).json({error: 'InvalidPermissions'});
  }

    //Revisar la url cuando está en producción...

}
