const config = require('./config');
const io = require('socket.io');
const sensor = require('./sockets/sensors');
const jwt = require('jsonwebtoken');


module.exports = class socket {

    constructor(server) {
        this.io = io(server);
        // scoket.io middleware
        this.io.use((socket, next) => {
            const token = socket.handshake.query.token;
            // verify token
            jwt.verify(token, config.secretKey, (err, decoded) => {
                //if (err) return next(err);
                //socket._id = decoded._id;
                next();
            });
        });

    }

    listen() {
        this.io.on('connection', sensor);
    }
}


