var r = require('./rethinkdbdash');

exports.get = (table, data) => {
    return new Promise((resolve, reject) => {
        try{
            let m=0, n=-1, properties;
            // Paginación
            if(data.pageNumber&&data.pageSize){
                m=(data.pageNumber - 1) * data.pageSize;
                n=data.pageNumber * data.pageSize-1;
                delete data.pageNumber;
                delete data.pageSize;
            }
            // Filtro para propiedades del objeto que se desean devolver.
            if(data.properties){
                properties=data.properties;
                delete data.properties;
            }
            // Permite utilizar filtro para elementos activos o borrados.
            if(data.active === undefined) {data.active=true;}
            // Filtro para fecha de inicio ficha de fin de query.
            let dateFilter = {};
            if(data.startDate && data.endDate){
                dateFilter = r.row('createdOn').during(r.ISO8601(data.startDate), r.ISO8601(data.endDate));
                delete data.startDate;
                delete data.endDate;
            } else if (data.startDate) {
                dateFilter = r.row('createdOn').during(r.ISO8601(data.startDate), r.now());
                delete data.startDate;
            }
            // Filtro utilizando las propiedades específicas del objeto.
            var filterBy=data;
            // Comienza la petición.
            r.table(table)
            .filter(filterBy)
            .filter(dateFilter)
            .map((item) => {
                if(properties){
                    var result={};
                    properties.forEach(element => {
                        result[element] = item(element);
                    });
                    return result;
                }
                else{return item};
            })
            .orderBy('createdOn')
            .coerceTo('array')
            .do((doc) => {
                return {
                    items:doc.slice(m,n,{rightBound:'closed'}),
                    totalFound:doc.count()
                };
            })
            .run()
            .then((result) => {  
                resolve(result);
            })
            .error((err) => {
                console.log('common.get rethink-error; table: %s, data:', table, data,', err:', err);
                reject(err);
            });
        } catch (err){
            console.log('common.get catch-error; table: %s, data:', table, data,', err:', err);
            reject(err);
        }
    });
};

exports.getById = (table, id) => {
    return new Promise((resolve, reject) => {
        try{
            r.table(table)
            .get(id)
            .run()
            .then((resultInsert) => {
                console.log(resultInsert);
                resolve(resultInsert);
            })
            .error((err) => {
                reject(err);
            });
        } catch (err){
            reject(err);
        }
    });
};

// Devuelve las propiedades de una tabla.
exports.getProperties = (table) => {
    return new Promise((resolve, reject) => {
        try{
            r.table(table)
            .map((doc) => {
                return doc.keys();
            })
            .reduce((uniq, doc) => {
                return uniq.setUnion(doc);
            })
            .distinct()
            .run()
            .then((result) => {  
                resolve(result);
            })
            .error((err) => {
                console.log('common.get rethink-error; table: %s, data:', "clients", searchItem,', err:', searchItem);
                reject(err);
            });
            
        } catch (err){
            console.log('common.get catch-error; table: %s, data:', "clients", searchItem,', err:', searchItem);
            reject(err);
        }
    });
};