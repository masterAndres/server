var r = require('./rethinkdbdash');

exports.get = (table, data) => {
    return new Promise((resolve, reject) => {
        try{
            let m=0, n=-1;
            let  dateFilter = r.row('createdOn').during(r.now().sub(60*data.min), r.now());
            let N=data.min==15?90:data.min==30?180:data.min==60?360:data.min==120?720:1800;
            let size= Math.ceil((0.9604*N)/((0.0025*(N-1))+0.9604));
            // Comienza la petición.
            r.table(table)
            .filter({sensorId:data.id})
            .filter(dateFilter)
            .sample(size)
            .orderBy('createdOn')
            .coerceTo('array')
            .do((doc) => {
                return {
                    items:doc.slice(m,n,{rightBound:'closed'}),
                    totalFound:doc.count()
                };
            })
            .run()
            .then((result) => {  
                resolve(result);
            })
            .error((err) => {
                console.log('common.get rethink-error; table: %s, data:', table, data,', err:', err);
                reject(err);
            });
        } catch (err){
            console.log('common.get catch-error; table: %s, data:', table, data,', err:', err);
            reject(err);
        }
    });
};

exports.all = (table) => {
    return new Promise((resolve, reject) => {
        try{
            let m=0, n=-1;
            // Comienza la petición.
            r.table(table)
            .pluck(['sensorId','sensor']).limit(10).distinct()
            .coerceTo('array')
            .do((doc) => {
                return {
                    items:doc.slice(m,n,{rightBound:'closed'}),
                    totalFound:doc.count()
                };
            })
            .run()
            .then((result) => {  
                resolve(result);
            })
            .error((err) => {
                console.log('common.get rethink-error; table: %s, data:', table, data,', err:', err);
                reject(err);
            });
        } catch (err){
            console.log('common.get catch-error; table: %s, data:', table, data,', err:', err);
            reject(err);
        }
    });
}

exports.insert = (table, insertData) => {
    return new Promise((resolve, reject) => {
        try{
            insertData.active = true;
            insertData.createdOn = r.now();
            r.table(table)
            .insert(insertData, {returnChanges: true})
            .run()
            .then((resultInsert) => {
                if (resultInsert.errors) {
                    reject(resultInsert.first_error);
                } else {
                    resolve(resultInsert.changes[0].new_val);
                }
            })
            .error((err) => {
                console.log('common.create rethink-error; table: %s, insertData:', table, insertData,', err:', err);
                reject(err);
            });
        } catch (err){
            console.log('common.create catch-error; table: %s, insertData:', table, insertData,', err:', err);
            reject(err);
        }
    });
};