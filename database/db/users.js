const r = require('./rethinkdbdash');
const crypto = require('crypto');
const config = require('../../config');
const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

/**
 * generates random string of characters i.e salt
 * @function
 * @param {number} length - Length of the random string.
 */
genSalt = (length) => {
    return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex') /** convert to hexadecimal format */
        .slice(0, length);   /** return required number of characters */
};

/**
 * hash password with sha512.
 * @function
 * @param {string} password - List of required fields.
 * @param {string} salt - Data to be validated.
 */
sha512 = (password, salt) => {
    var hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hash.update(password);
    var value = hash.digest('hex');
    return value;
};

exports.genSalt = genSalt;

exports.sha512 = sha512;

exports.existUserByUsername = (username) => {
    return new Promise((resolve, reject) => {
        r.table('users')
            .filter({ username: username })
            .map((user) => {
                return {
                    id: user('id'),
                    name: user('name'),
                    permissions: r.table('roles').get((user('roleId'))).getField('permissions'),
                    active: user('active')
                }
            })
            .coerceTo('array')
            .run().then((result) => {
                if (result.length > 0) {
                    resolve(result[0]);
                } else {
                    resolve(false);
                }
            }).error((err) => {
                console.log(err);
                reject(err);
            });
    });
};

exports.validateUser = (id, password, active) => {
    return new Promise((resolve, reject) => {
        r.table('users')
            .get(id)
            .run().then((user) => {
                resolve({
                    password: user.password == this.sha512(password, user.salt),
                    active: user.active === true
                });
            }).error((err) => {
                console.log(err);
                reject(err);
            });
    });
};

exports.get = (table, data) => {
    return new Promise((resolve, reject) => {
        try{
            var m=0, n=-1, properties;
            if(data.pageNumber&&data.pageSize){
                m=(data.pageNumber - 1) * data.pageSize;
                n=data.pageNumber * data.pageSize-1;
                delete data.pageNumber;
                delete data.pageSize;
            }
            if(data.properties){
                properties=data.properties;
                delete data.properties;
            }
            if(data.isDeleted === undefined) {data.isDeleted=false;}
            let dateFilter = {};
            if(data.startDate && data.endDate){
                dateFilter = r.row('createdOn').during(r.ISO8601(data.startDate), r.ISO8601(data.endDate));
                delete data.startDate;
                delete data.endDate;
            } else if (data.startDate) {
                dateFilter = r.row('createdOn').during(r.ISO8601(data.startDate), r.now());
                delete data.startDate;
            }
            var filterBy=data;
            r.table(table)
            .filter(filterBy)
            .filter(dateFilter)
            .map((item) => {
                if(properties){
                    var result={};
                    properties.forEach(element => {
                        result[element] = item(element);
                    });
                    return result;
                }
                else{return item};
            })
            .without('salt','password')
            .orderBy('id')
            .coerceTo('array')
            .do((doc) => {
                return {
                    items:doc.slice(m,n,{rightBound:'closed'}),
                    totalFound:doc.count()
                };
            })
            .run()
            .then((result) => {  
                resolve(result);
            })
            .error((err) => {
                console.log('common.get rethink-error; table: %s, data:', table, data,', err:', err);
                reject(err);
            });
        } catch (err){
            console.log('common.get catch-error; table: %s, data:', table, data,', err:', err);
            reject(err);
        }
    });
};

exports.create = (table, insertData) => {
    return new Promise((resolve, reject) => {
        try{
            insertData.active=true;
            insertData.isDeleted=false;
            insertData.createdOn=new Date();
            if(insertData.password){
                insertData.salt = genSalt(16);
                insertData.password = sha512(insertData.password, insertData.salt);
            }
            r.table(table)
            .insert(insertData, {returnChanges: true})
            .run()
            .then((resultInsert) => {
                if (resultInsert.errors) {
                    reject(resultInsert.first_error);
                } else {
                    delete resultInsert.changes[0].new_val.salt;
                    delete resultInsert.changes[0].new_val.password;
                    resolve(resultInsert.changes[0].new_val);
                }
            })
            .error((err) => {
                console.log('common.create rethink-error; table: %s, insertData:', table, insertData,', err:', err);
                reject(err);
            });
        } catch (err){
            console.log('common.create rethink-error; table: %s, insertData:', table, insertData,', err:', err);
            reject(err);
        }
    });
};
