const r = require('./db/rethinkdbdash');
const config = require('../config');
const User = require('./db_classes/User');
const Role = require('./db_classes/Role');

//#region Defaults (requiere un constructor para el superAdmin, a saber, el "new user()")
          // En el primer bloque se definen los
          // objetos que deben estar sí o sí,
          // tales como los permissions, roles y el usuario superAdmin.

const rethinkdb = config.rethinkdb;
const dbname    = config.rethinkdb.db;

//Users 
//get by ID

var Data = {
  roles:        [],
  users:        [],
  logs:         [],
  sensor:       []
};

//#region Creando objetos
Data.users.push(new User());
Data.roles.push(new Role());
//#endregion

//#region Insertando
var tables = Object.keys(Data);
var controlConnection = [];
for (let key in tables) controlConnection.push(0);

check();

function check() {
  r.connect(rethinkdb).then((conn) => {
    r.dbList().run(conn).then((result) => {
      if (result.indexOf(dbname) == -1) {
        console.log("Creando base de datos...\n");
        create(conn);
      } else {
        console.log("Ya existe, eliminando base...\n");
        r.dbDrop(dbname).run(conn).then((result) => {
          create(conn);
        }).error((error) => {
          console.log(error);
        });
      }
    }).error((error) => {
      console.log(error);
    })

    closeConnection(conn);


  }).error((error) => {
    console.log(error);
  });

  function closeConnection(conn) {
    setTimeout(() => {
      let band = 1;
      for (let key in controlConnection) band = band * controlConnection[key];
      if (band) {
        console.log('\nClose connection\n');
        conn.close();
        process.exit();
      } else {
        closeConnection(conn);
      }
    }, 100);
  }
}

function create(conn) {
  r.dbCreate(dbname).run(conn).then((result) => {
    var tableCount=0;
    tables.forEach((element, index) => {
      r.db(dbname).tableCreate(element).run(conn).then((result) => {
        tableCount++;
        console.log((tableCount+100).toString().slice(-2)+'/'+tables.length+' se creo la tabla ' + element);
        if(Data[element].length){
          var tantos=(Data[element].length+100000).toString().slice(-5).replace(/^0+/,(x) => {var b='';for(var i=0;i<x.length;i++){b+=' '}return b});
          r.db(dbname).table(element)
          .insert(Data[element]).run(conn).then((result) => {
              controlConnection[index]++;
              console.log('\t'+tantos+' datos insertados en la tabla ' + element);
          }).error((err) => { console.log(err); });
        } else controlConnection[index]++;
      }).error((err) => { console.log(err); });
    }, this);
  }).error((err) => { console.log(err); })
};
//#endregion