const r = require('../db/rethinkdbdash');

module.exports = class Role {
  constructor(
    name = "Admin",
    permissions = {"all": true, "users": true}
  ) {
    this.id = r.uuid(name);
    this.name = name;
    this.permissions = permissions;

    this.active = true;
    this.createdOn = r.now();
    this.modifiedOn = r.now();
  }
}
