const r = require('../db/rethinkdbdash');

module.exports = class User {
  constructor(
    password = "26dbe8649a2944d09348c0e40554d63bf6e6931b01551ce4886e2040ee986953187bf400fa4537c74a332fd46bb99734f3a8507ffd95722b41036ecda35149bb", // UptPD74PA
    role = "Admin",
    salt = "4787417516b00cad",
    username = "PruebasIot",
    name = "Admin de pruebas",
    phone, birthdate
  ) {
    this.password = password;
    this.roleId = r.uuid(role);
    this.salt = salt;
    this.username = username;
    this.name = name;
    this.phone = phone;
    this.birthdate = birthdate;

    this.active = true;
    this.isDeleted = false;
    this.createdOn = r.now();
    this.modifiedOn = r.now();
  }
}
